console.log("Hello World"); 


//3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)

let anyNum = 1;
getCube = anyNum ** 3;


//4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…

console.log(`The cube of ${anyNum} is ${getCube}.`)


//5. Create a variable address with a value of an array containing details of an address.

const address = ["258 Washington Ave NW", "California", "90011"];


//6. Destructure the array and print out a message with the full address using Template Literals.

const [street, state, zipCode] = address;
// console.log(street);
// console.log(state);
// console.log(zipCode);
console.log(`I live at ${street}, ${state} ${zipCode}`)


//7. Create a variable animal with a value of an object data type with different animal details as it’s properties.

const animal = {
	lolong: "saltwater crocodile",
	weight: "1075 kgs",
	measure: "20ft 3 in"
}


//8. Destructure the object and print out a message with the details of the animal using Template Literals.

const {lolong, weight, measure} = animal;
console.log(`Lolong was a ${lolong}. He weighed at ${weight} with a measurement of ${measure}.`)


//9. Create an array of numbers.

// let numArr = [1,2,3,4,5];
// console.log(numArr[0]);
// console.log(numArr[1]);
// console.log(numArr[2]);
// console.log(numArr[3]);
// console.log(numArr[4]);

const numArr = [1,2,3,4,5];


//10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.

numArr.forEach(numArr => 
	console.log(`${numArr}`)
)


//11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.

// const compute = (v, w, x, y, z) => v + w + x + y + z;
// let num = compute(1,2,3,4,5);
// console.log(num);

// students.forEach(student => {
// 	console.log(`${student} is a student.`)

let reducedNumArr = numArr.reduce((x, y) => x + y);
	console.log(reducedNumArr);


//12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.

class dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}


// 13. Create/instantiate a new object from the class Dog and console log the object.

const myDog = new dog();
myDog.name = "Yasu";
myDog.age = "5 yrs";
myDog.breed = "Shih Tzu";
console.log(myDog);



